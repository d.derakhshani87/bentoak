from django.apps import AppConfig
from happyreport.ioc import container
from . import views


class WebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'web'

    def ready(self):
        container.wire(modules=[views])
