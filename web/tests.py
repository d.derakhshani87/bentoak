from django.test import TestCase

# Create your tests here.
from unittest import mock

from django.urls import reverse
from django.test import TestCase
from happyreport.ioc.services import DataService

from happyreport.ioc import container


class IndexTests(TestCase):

    def test_index(self):
        data_service_mock = mock.Mock(spec=DataService)

    def test_main_report(self):
        url = reverse("web:main-report")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_regional_report(self):
        url = reverse("web:regional_report")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_yearly_report(self):
        url = reverse("web:yearly-report")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
