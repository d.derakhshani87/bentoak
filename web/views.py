from django.shortcuts import render
from django.http import HttpResponse
from dependency_injector.wiring import inject, Provide

from happyreport.ioc.containers import Container
from happyreport.ioc.services import DataService
from happyreport.file.excelreader import ExcelReader

colors = ['#95348F', '#EC008C', '#FD642B', '#DCF400',
          '#65C9D1', '#848CC6', '#BB9CCA', '', '']

# Main View to display data


def home(request,
         data_service: DataService = Provide[Container.data_service],):
    data, _ = data_service.loadData()

    return render(request, "main-report.html", {'model': data, 'colors': colors})


# report by regions
def regional_report(request,
                    data_service: DataService = Provide[Container.data_service],):

    data, region = data_service.loadData()

    return render(request, "regional-report.html", {'model': region,  'colors': colors})


def yearly_report(request,
                  data_service: DataService = Provide[Container.data_service],):

    data, years = data_service.loadYearlyData()

    if request.method == 'POST':
        # get  year filter
        year = request.POST.get('years-filter')
        # filter the data by year
        data = data_service.queryDataByYear(year)
    else:
        # set to smallest year
        year = 'No year selected'

    return render(request, "yearly-report.html", {'model': data, 'years': years, 'year': year, 'colors': colors})
