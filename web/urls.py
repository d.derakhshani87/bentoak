from django.urls import path
from . import views
app_name = "web"

urlpatterns = [
    path("main-report", views.home, name="main-report"),
    path("regional_report", views.regional_report, name="regional_report"),
    path("yearly-report", views.yearly_report, name="yearly-report")
]
