<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->




<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">BentOak </h3>

  <p align="center">
    This is sample file reader and report for BentOak System
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<



<!-- ABOUT THE PROJECT -->
## About The Project
 
 <span>The project is a test project written for Bentoak systems for evaluation purpose.
 </span>
 <br>
 The project use Depenency Injection for reading files. DI configured in ioc folder. 
  <br>
 DI create singlton instance of filereader and inject excelreader as filereader into views.
  <br>
 Reading CSV file and query data implemented by panda.

<p>
   <img src="screenshots/01.png" alt="screenshot" height="400" >
   <img src="screenshots/02.png" alt="screenshot" height="400" >
<p>

### Built With


* [Python](https://getbootstrap.com)
* [Django](https://jquery.com)
* [pipenv](https://jquery.com)
* [Panda](https://laravel.com)



<!-- GETTING STARTED -->
## Getting Started

Install Python and follow Installation section.
Please do not use docker, Containerized version has not been tested. I will make update whenever it will be ready.


### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* Python
  ```sh
  install python via https://www.python.org/
  ```

### Installation

1. Get ready for next steps :) 
2. Clone the repo
   ```sh
   git clone https://gitlab.com/d.derakhshani87/bentoak/
   ```
3. Install pipenv
   ```sh
   pip install pipenv
   ```
4. Update dependecies
   ```sh
   pipenv update
   ```

5. run project
   ```sh
   python manage.py runserver
   ```

6. Open follwing page address
   ```sh
   http://127.0.0.1:8000/app/main-report
   ```



<!-- USAGE EXAMPLES -->
## Usage

After running project open app/main-report there are three button on main page you can navigate to diffrent reports via them.


<!-- CONTRIBUTING -->
## Contributing
 This project is not intented for contribution



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.




