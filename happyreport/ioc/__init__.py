from .containers import Container
from happyreport import settings

container = Container()
container.config.from_dict(settings.__dict__)
