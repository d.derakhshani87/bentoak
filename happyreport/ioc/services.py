
from happyreport.file.filereader import FileReader
from happyreport.models.coutryreport import CountryReport
from happyreport.models.region import Region


class DataService:
    """Load Data service performs reading excel file by Excel File Reader."""

    def __init__(self, filereader: FileReader):
        self.filereader = filereader
        self.data_years = None
        self.data = None

    def loadData(self):
        # load data from excel if there is no cached data(data service is singlton in IOC)
        if self.data is None:
            self.data = self.filereader.readFile(
                'world-happiness-report-2021.csv')

        # sort the data by Ladder_score descending
        self.data = self.data.sort_values(by=['Ladder_score'], ascending=False)

        # get all unique regionals
        self.regions = self.data['Regional_indicator'].unique().tolist()

        regions = {}
        for item in self.regions:
            # create country object(country name,ladder_Score,all explained columns start at 13)
            region = Region(item)
            regions[item] = region

        countries = []
        for item in self.data.values.tolist():
            # create country object(country name,ladder_Score,all explained columns start at 13)
            country = CountryReport(item[0], item[2], item[13:])
            regions[item[1]].countries.append(country)
            countries.append(country)

        region_list = []
        for key, value in regions.items():
            region_list.append(value)

        return countries, region_list

    def loadYearlyData(self):
        # load data from excel if there is no cached data(data service is singlton in IOC)
        if self.data_years is None:
            self.data_years = self.filereader.readFile(
                'world-happiness-report.csv')

        # sort the data by Ladder_score descending
        self.data_years = self.data_years.sort_values(
            by=['Life_Ladder'], ascending=False)

        # get all unique regionals
        self.years = self.data_years['year'].unique().tolist()

        countries = self.createModelYear(self.data_years)

        return countries, self.years

    # Query Yearly Data by year
    def queryDataByYear(self, year):
        data = self.data_years.copy()
        data.query('year == '+year, inplace=True)

        countries = self.createModelYear(data)
        return countries

    def createModelYear(self, data):
        countries = []
        for item in data.values.tolist():
            # create country object(country name,ladder_Score,all explained columns start at 13)
            country = CountryReport(item[0], item[2], item[3:])
            countries.append(country)
        return countries
