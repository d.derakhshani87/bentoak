
from dependency_injector import containers, providers
from .services import DataService
from happyreport.file.excelreader import ExcelReader


class Container(containers.DeclarativeContainer):

    config = providers.Configuration()

    """File Reader must be single instance"""
    excelReader = providers.Singleton(
        ExcelReader
    )

    """Inject Excel Reader as file reader to FileService"""
    data_service = providers.Singleton(
        DataService,
        filereader=excelReader,
    )
