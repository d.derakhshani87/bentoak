from abc import ABC, abstractmethod


class FileReader(ABC):
    @abstractmethod
    def readFile(self, filePath):
        pass
