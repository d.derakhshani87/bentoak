from .filereader import FileReader
from xlrd import open_workbook
import pandas as pd
import os
from happyreport.settings import BASE_DIR


class ExcelReader(FileReader):

    def readFile(self, filePath):
        # prepare data folder path relative to BASE_DIR
        fullPath = os.path.join(BASE_DIR, 'data', filePath)
        # read file as csv
        df = pd.read_csv(fullPath, encoding="utf-8")

        # replacing blank spaces with '_'
        df.columns = [column.replace(" ", "_") for column in df.columns]

        # return panda data frame
        return df
