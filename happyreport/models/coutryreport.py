from .region import Region
import numpy as np


class CountryReport:
    # Class attribute

    def __init__(self, countryName, ladder_score, scores):
        super().__init__()
        self.countryName = countryName
        self.ladder_score = ladder_score
        # make all score to scale 100
        self.scores = np.array(scores) * 10
        self.region = None
